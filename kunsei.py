#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
from logging.handlers import RotatingFileHandler
from yelp.fusion import Fusion

def create_file_handler(filename):
    # ハンドラ作成：10MBのサイズローテートで5世代管理
    handler = RotatingFileHandler(
        filename=filename,
        maxBytes=10000000,
        backupCount=5,
        encoding='utf-8'
    )
    formatter = logging.Formatter('%(asctime)s %(name)s [%(levelname)s] %(message)s')
    handler.setFormatter(formatter)
    return handler

def set_root_logger(handler, is_debug=False):
    """rootロガーを設定する

    Args:
        handler(StreamHandler or FileHandler)
        is_debug(bool): デバッグモードならTrue
    """
    # rootロガー取得
    logger = logging.getLogger()
    # rootロガー設定
    if is_debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    logger.addHandler(handler)

if __name__ == '__main__':
    api_key = os.getenv('API_KEY')
    is_debug = True
    os.makedirs('log', exist_ok=True)
    log_file_name = os.path.join('log', 'kunsei.log')
    set_root_logger(create_file_handler(log_file_name), is_debug)
    f = Fusion(api_key)
    term = 'ランチ'
    location = '京橋'
    f.business_search_by_location(location, term)
