#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import logging

class Fusion(object):
    def __init__(self, api_key):
        self.base_url = 'https://api.yelp.com'
        self.headers = {
            'Authorization': f'Bearer {api_key}'
        }
        self.logger = logging.getLogger(__name__)
    
    def business_search_by_location(self, location, term):
        """
        Args:
        """
        path = f"{self.base_url}/v3/businesses/search"
        url_params = {
            'location': location,
            'term': term
        }
        logging.info(path)
        response = requests.get(path, headers=self.headers, params=url_params)
        logging.info(response.status_code)
        logging.info(response.json())
